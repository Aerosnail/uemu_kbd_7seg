#include <string.h>

#include "gui/nuklear/nuklear.h"
#include "keyboard.h"

int
keyboard_init(struct keyboard *self, const char *name)
{
	(void)name;
	memset(self->matrix, 0, sizeof(self->matrix));
	return 0;
}

int
keyboard_deinit(struct keyboard *self)
{
	(void)self;
	return 0;
}

int
keyboard_render(struct keyboard *self, struct nk_context *ctx)
{
	nk_button_push_behavior(ctx, NK_BUTTON_REPEATER);

	nk_layout_row_dynamic(ctx, 60, 6);
	self->matrix[1][1] = nk_button_label(ctx, "C");
	self->matrix[1][0] = nk_button_label(ctx, "D");
	self->matrix[2][6] = nk_button_label(ctx, "E");
	self->matrix[2][5] = nk_button_label(ctx, "F");
	self->matrix[2][0] = nk_button_label(ctx, "PC");

	nk_layout_row_dynamic(ctx, 60, 6);
	self->matrix[1][5] = nk_button_label(ctx, "8");
	self->matrix[1][4] = nk_button_label(ctx, "9");
	self->matrix[1][3] = nk_button_label(ctx, "A");
	self->matrix[1][2] = nk_button_label(ctx, "B");
	self->matrix[2][4] = nk_button_label(ctx, "AD");
	self->matrix[2][1] = nk_button_label(ctx, "GO");

	nk_layout_row_dynamic(ctx, 60, 6);
	self->matrix[0][2] = nk_button_label(ctx, "4");
	self->matrix[0][1] = nk_button_label(ctx, "5");
	self->matrix[0][0] = nk_button_label(ctx, "6");
	self->matrix[1][6] = nk_button_label(ctx, "7");
	self->matrix[2][3] = nk_button_label(ctx, "DA");
	nk_button_label(ctx, "ST");             /* TODO */


	nk_layout_row_dynamic(ctx, 60, 6);
	self->matrix[0][6] = nk_button_label(ctx, "0");
	self->matrix[0][5] = nk_button_label(ctx, "1");
	self->matrix[0][4] = nk_button_label(ctx, "2");
	self->matrix[0][3] = nk_button_label(ctx, "3");
	self->matrix[2][2] = nk_button_label(ctx, "+");
	self->reset = nk_button_label(ctx, "RST");            /* TODO */

	nk_button_pop_behavior(ctx);

	return 0;
}

int
keyboard_get_state(struct keyboard *self, int row, int col)
{
	return self->matrix[row][col];
}

int
keyboard_get_reset(struct keyboard *self)
{
	return self->reset;
}

#pragma once

#include <stdint.h>
#include <stdlib.h>

#include "gui/nuklear/nuklear.h"

struct display {
	uint8_t data[6];    /* 7 bits per segment */

	float segments[6][7];
	float persistence;
};

int display_init(struct display *self, const char *name);
int display_deinit(struct display *self);
int display_render(struct display *self, struct nk_context *ctx);

#include <assert.h>
#include <errno.h>
#include <string.h>

#include <utils.h>
#include <dtb/node.h>

#include "devicetree.h"

enum props {
	PROP_MUXSEL_GPIOS = 0,
	PROP_MUXDATA_GPIOS = 1,

	PROP_COUNT
};

static const char *_prop_names[] = {
	[PROP_MUXSEL_GPIOS] = "muxsel-gpios",
	[PROP_MUXDATA_GPIOS] = "muxdata-gpios",
};

int
parse_dtb_prop(struct kbd_7seg_specs *dst, const struct dtb_property *prop,
               find_phandle_t find_phandle, void *find_phandle_ctx)
{
	const struct dtb_node *phandle_node;
	const struct dtb_property *gpio_cells_prop;
	size_t i, j;
	size_t gpio_num;
	uint32_t phandle, gpio_cells;
	uint32_t tmp;
	char pin_name[32];

	for (i = 0; i < PROP_COUNT; i++) {
		if (!strcmp(prop->name, _prop_names[i])) {
			break;
		}
	}

	switch (i) {
	case PROP_MUXSEL_GPIOS:
		gpio_num = 0;
		for (i = 0; i < prop->len && gpio_num < LEN(dst->mux_sel_dst); gpio_num++) {
			/* Get phandle */
			dtb_property_get_u32(&phandle, prop, i / 4, 1);
			i += 4;

			/* Get #gpio-cells value for module with the given phandle */
			phandle_node = find_phandle(find_phandle_ctx, phandle);
			gpio_cells_prop = dtb_node_find_property(phandle_node, "#gpio-cells");
			if (!gpio_cells_prop) return ENOKEY;

			dtb_property_get_u32(&gpio_cells, gpio_cells_prop, 0, 1);

			/* Get pin name */
			pin_name[0] = 0;
			for (j = 0; j < gpio_cells; j++) {
				dtb_property_get_u32(&tmp, prop, i / 4, gpio_cells);
				i += 4;

				/* Append pin index to name */
				sprintf(pin_name + strlen(pin_name), "%d:", tmp);
			}

			if (pin_name[0]) {
				pin_name[strlen(pin_name) - 1] = 0;
			}

			dst->mux_sel_dst[gpio_num] = gpio_find_by_name(phandle_node->name, pin_name);
		}
		assert(gpio_num == LEN(dst->mux_sel_dst));

		break;

	case PROP_MUXDATA_GPIOS:
		gpio_num = 0;
		for (i = 0; i < prop->len && gpio_num < LEN(dst->mux_data_dst); gpio_num++) {
			/* Get phandle */
			dtb_property_get_u32(&phandle, prop, i / 4, 1);
			i += 4;

			/* Get #gpio-cells value for module with the given phandle */
			phandle_node = find_phandle(find_phandle_ctx, phandle);
			gpio_cells_prop = dtb_node_find_property(phandle_node, "#gpio-cells");
			if (!gpio_cells_prop) return ENOKEY;

			dtb_property_get_u32(&gpio_cells, gpio_cells_prop, 0, 1);

			/* Get pin name */
			pin_name[0] = 0;
			for (j = 0; j < gpio_cells; j++) {
				dtb_property_get_u32(&tmp, prop, i / 4, gpio_cells);
				i += 4;

				/* Append pin index to name */
				sprintf(pin_name + strlen(pin_name), "%d:", tmp);
			}

			if (pin_name[0]) {
				pin_name[strlen(pin_name) - 1] = 0;
			}

			dst->mux_data_dst[gpio_num] = gpio_find_by_name(phandle_node->name, pin_name);
		}
		assert(gpio_num == LEN(dst->mux_data_dst));
		break;

	default:
		return ENOKEY;
	}

	return 0;
}

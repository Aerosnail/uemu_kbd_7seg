#pragma once

#include <module.h>

#include "kbd_7seg.h"

int parse_dtb_prop(struct kbd_7seg_specs *dst, const struct dtb_property *prop,
                   find_phandle_t find_phandle, void *find_phandle_ctx);

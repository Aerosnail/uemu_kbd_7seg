#include <pthread.h>
#include <SDL2/SDL.h>
#include <stdbool.h>

#include <gpio.h>
#include <log/log.h>
#include <utils.h>

#include "gui/gui.h"
#include "kbd_7seg.h"

static int on_muxsel_change(struct kbd_7seg *self, int value);
static int on_muxdata_change(struct kbd_7seg *self, int value);

int
kbd_7seg_init(struct kbd_7seg **dst, const char *module_name)
{
	struct kbd_7seg *self = calloc(1, sizeof(*self));
	char pin_name[32];
	size_t i;
	int ret;

	self->selector = 0;
	self->data = 0;

	/* Initialize sub-views */
	ret = display_init(&self->display, module_name);
	if (ret) return ret;
	ret = keyboard_init(&self->keyboard, module_name);
	if (ret) return ret;

	/* Connect GPIOs to their backing store */
	for (i=0; i<LEN(self->mux_sel); i++) {
		sprintf(pin_name, "muxsel:%d", (int)i);
		self->mux_sel[i] = gpio_create_instance(module_name,
		                                        pin_name,
		                                        (int(*)(void*,int))on_muxsel_change,
		                                        self);
	}
	for (i=0; i<LEN(self->mux_data); i++) {
		sprintf(pin_name, "muxdata:%d", (int)i);
		self->mux_data[i] = gpio_create_instance(module_name,
		                                         pin_name,
		                                         (int(*)(void*,int))on_muxdata_change,
		                                         self);
	}


	self->reset = gpio_create_instance(module_name, "rst", NULL, NULL);
	gpio_write(self->reset, 1);
	gpio_set_type(self->reset, GPIO_TYPE_OUTPUT_PP);

	gui_init(self);

	*dst = self;
	return 0;
}

int
kbd_7seg_deinit(struct kbd_7seg *self)
{
	size_t i;
	int ret = 0;

	gui_deinit(self);

	/* Deregister pins */
	for (i=0; i<LEN(self->mux_sel); i++) {
		ret |= gpio_delete_instance(self->mux_sel[i]);
	}
	for (i=0; i<LEN(self->mux_data); i++) {
		ret |= gpio_delete_instance(self->mux_data[i]);
	}

	ret |= display_deinit(&self->display);
	ret |= keyboard_deinit(&self->keyboard);

	free(self);

	return ret;
}

/* Static functions {{{ */

static int
on_muxsel_change(struct kbd_7seg *self, int value)
{
	int_fast8_t selector = 0;
	uint8_t data = 0;
	size_t i;

	(void)value;

	/* Update selector */
	for (i=0; i<LEN(self->mux_sel); i++) {
		selector |= gpio_read(self->mux_sel[i]) << i;
	}

	switch (selector) {
	case 0: /* keypad row 2 */
	case 1: /* keypad row 1 */
	case 2: /* keypad row 0 */
		for (i=0; i<LEN(self->mux_data); i++) {
			value = keyboard_get_state(&self->keyboard, selector, i);

			gpio_set_type(self->mux_data[i], GPIO_TYPE_OUTPUT_PP);
			gpio_write(self->mux_data[i], !value);
		}
		break;

	case 3: /* not connected */
		break;

	case 4: /* display digit 1 */
	case 5: /* display digit 2 */
	case 6: /* display digit 3 */
	case 7: /* display digit 4 */
	case 8: /* display digit 5 */
	case 9: /* display digit 6 */
		for (i=0; i<LEN(self->mux_data); i++) {
			gpio_set_type(self->mux_data[i], GPIO_TYPE_INPUT);
			data |= gpio_read(self->mux_data[i]) << i;
		}
		self->display.data[selector - 4] = ~data;
		break;

	default:
		break;
	}

	return 0;
}

static int
on_muxdata_change(struct kbd_7seg *self, int value)
{
	int_fast8_t selector = 0;
	uint8_t data = 0;
	size_t i;

	(void)value;

	/* Update selector */
	for (i=0; i<LEN(self->mux_sel); i++) {
		selector |= gpio_read(self->mux_sel[i]) << i;
	}

	switch (selector) {
	case 0: /* keypad row 2 */
	case 1: /* keypad row 1 */
	case 2: /* keypad row 0 */
		break;

	case 3: /* not connected */
		break;

	case 4: /* display digit 1 */
	case 5: /* display digit 2 */
	case 6: /* display digit 3 */
	case 7: /* display digit 4 */
	case 8: /* display digit 5 */
	case 9: /* display digit 6 */
		for (i=0; i<LEN(self->mux_data); i++) {
			gpio_set_type(self->mux_data[i], GPIO_TYPE_INPUT);
			data |= gpio_read(self->mux_data[i]) << i;
		}
		self->display.data[selector - 4] = ~data;
		break;

	default:
		break;
	}

	return 0;
}
/* }}} */

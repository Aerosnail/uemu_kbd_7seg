#pragma once

#include "nuklear/nuklear.h"

void draw_7seg(struct nk_context *ctx, struct nk_vec2 pos, struct nk_vec2 e, uint8_t mask);
void draw_7seg_float(struct nk_context *ctx, struct nk_vec2 pos, struct nk_vec2 e, float segments[7]);

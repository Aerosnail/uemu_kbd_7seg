#pragma once

#include "kbd_7seg.h"

#define WINDOW_TITLE "Keypad + display"
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600


int gui_init(struct kbd_7seg *kbd_7seg);
int gui_deinit(struct kbd_7seg *kbd_7seg);

#include "../glad/glad.h"
#include <SDL2/SDL.h>

#define NK_SDL_GL3_IMPLEMENTATION
#define NK_IMPLEMENTATION
#include "nuklear.h"
#include "nuklear_sdl_gl3.h"

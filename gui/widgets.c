#include <stdbool.h>
#include <stdlib.h>

#include "widgets.h"

#ifndef LEN
#define LEN(x) (sizeof(x)/sizeof(*(x)))
#endif

#define COLOR_ON (nk_rgb(255, 255, 255))
#define COLOR_OFF (nk_rgb(40, 40, 40))

#define W 15
#define H 7.5
void
draw_7seg(struct nk_context *ctx, struct nk_vec2 pos, struct nk_vec2 e, uint8_t mask)
{
	float segments[7];
	size_t i;

	for (i=0; i<LEN(segments); i++) {
		if (mask & (1 << i)) {
			segments[i] = 1.0;
		} else {
			segments[i] = 0.0;
		}
	}

	draw_7seg_float(ctx, pos, e, segments);

}

void
draw_7seg_float(struct nk_context *ctx, struct nk_vec2 pos, struct nk_vec2 e,
		float segments[7])
{
	const struct nk_color color_on = { .r = 255, .g = 255, .b = 255, .a = 255 };
	const struct nk_color color_off = { .r = 40, .g = 40, .b = 40, .a = 255 };

	float r[7][4]={
		{-1, -1,  H,  H},
		{ 1, -1, -H,  H},
		{ 1,  0, -H, -H},
		{-1,  1,  H, -W*1.5},
		{-1,  0,  H, -H},
		{-1, -1,  H,  H},
		{-1,  0,  H, -H}
	};

	pos.x += e.x;
	pos.y += e.y;

	struct nk_command_buffer *cmd_buffer = nk_window_get_canvas(ctx);

	for (int i=0; i<7; i++){
		struct nk_color color = nk_rgb(
				segments[i] * color_on.r + (1.0f - segments[i]) * color_off.r,
				segments[i] * color_on.g + (1.0f - segments[i]) * color_off.g,
				segments[i] * color_on.b + (1.0f - segments[i]) * color_off.b);

		struct nk_vec2 a,b;
		if(i % 3 == 0) {
			a = nk_vec2(
					pos.x + r[i][0]*e.x + r[i][2],
					pos.y + r[i][1]*e.y + r[i][3] - H
			);

			b = nk_vec2(
					a.x+e.x*2-W,
					a.y+W
			);
		} else {
			a = nk_vec2(
					pos.x + r[i][0]*e.x + r[i][2] - H,
					pos.y + r[i][1]*e.y + r[i][3]
			);
			b = nk_vec2(
					a.x+W,
					a.y+e.y-W
			);
		}
		struct nk_vec2 q = nk_vec2(b.x-a.x, b.y-a.y);
		float s=W*0.6,u=s-H;

		if (q.x>q.y) {
			struct nk_vec2 pp[]={
				{a.x + u, a.y + q.y*.5f},
				{a.x + s, a.y},
				{b.x - s, a.y},
				{b.x - u, a.y + q.y*.5f},
				{b.x - s, b.y},
				{a.x + s, b.y}
			};

			nk_fill_polygon(cmd_buffer, (float*)pp, LEN(pp), color);
		} else {
			struct nk_vec2 pp[]={
				{a.x + q.x*.5f, a.y + u},
				{b.x, a.y + s},
				{b.x, b.y - s},
				{b.x - q.x*.5f, b.y - u},
				{a.x, b.y - s},
				{a.x, a.y + s}
			};
			nk_fill_polygon(cmd_buffer, (float*)pp, LEN(pp), color);
		}
	}
}

#include <pthread.h>

#include <log/log.h>

#include "gui.h"
#include "glad/glad.h"
#include "nuklear/nuklear.h"
#include "nuklear/nuklear_sdl_gl3.h"
#include "style.h"
#include "widgets.h"

#define MAX_VERTEX_MEMORY (512 * 1024)
#define MAX_ELEMENT_MEMORY (128 * 1024)

static void *gui_thread(void *args);

int
gui_init(struct kbd_7seg *self)
{
	/* Launch render thread (opens main window) */
	self->running = true;
	return pthread_create(&self->tid, NULL, gui_thread, self);
}

int
gui_deinit(struct kbd_7seg *self)
{
	/* Stop GUI thread */
	if (self->running) self->running = false;
	if (self->tid) {
		pthread_join(self->tid, NULL);
		self->tid = 0;
	}

	return 0;
}

static void
*gui_thread(void *args)
{
	struct kbd_7seg *self = (struct kbd_7seg*)args;

	struct nk_context *ctx;
	SDL_Window *win;
	SDL_GLContext glContext;
	SDL_Event evt;
	int width, height;
	int gl_error;
	const char *gl_version;

	/* Initialize SDL2 */
	SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "0");
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS)) {
		fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
		return NULL;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	/* Create new SDL window */
	win = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	                       WINDOW_WIDTH, WINDOW_HEIGHT,
	                       SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN |
	                       SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
	if (!win) {
		log_error("SDL window creation failed: %s\n", SDL_GetError());
		return NULL;
	}

	/* Create context and init GLAD */
	glContext = SDL_GL_CreateContext(win);
	if (!glContext) {
		log_error("SDL context creation failed: %s\n", SDL_GetError());
		return NULL;
	}

	gladLoadGLES2Loader(SDL_GL_GetProcAddress);

	ctx = nk_sdl_init(win);
	gui_load_fonts(ctx, 1.4);
	gui_set_style_default(ctx);


	/* Check results */
	gl_version = (const char*)glGetString(GL_VERSION);
	if (gl_version) {
		log_debug("Created OpenGL context: %s\n", gl_version);
	} else {
		log_error("Unable to create OpenGL context: %s (OpenGL status %d)\n",
				SDL_GetError(), glGetError());
		return NULL;
	}

	/* Get initial window width/height */
	SDL_GetWindowSize(win, &width, &height);

	while (self->running) {
		/* Just make sure that everything is still fine */
		gl_error = glGetError();
		if (gl_error) fprintf(stderr,"Detected OpenGL non-zero status: %d\n", gl_error);


		/* Handle inputs */
		nk_input_begin(ctx);
		while (SDL_PollEvent(&evt)) {
			switch (evt.type) {
			case SDL_QUIT:
				self->running = false;
				break;
			default:
				break;
			}

			nk_sdl_handle_event(&evt);
		}
		nk_input_end(ctx);

		/* Update window size */
		SDL_GetWindowSize(win, &width, &height);

		/* Main draw area */
		if (nk_begin(ctx, "Address + data", nk_rect(0, 0, width, height),
			NK_WINDOW_BORDER)) {

			nk_layout_row_dynamic(ctx, 200, 1);
			if (nk_group_begin(ctx, "display", NK_WINDOW_SCROLL_AUTO_HIDE)) {
				display_render(&self->display, ctx);

				nk_group_end(ctx);
			}

			nk_layout_row_dynamic(ctx, 280, 1);
			if (nk_group_begin(ctx, "keyboard", NK_WINDOW_SCROLL_AUTO_HIDE)) {
				keyboard_render(&self->keyboard, ctx);

				gpio_write(self->reset, !keyboard_get_reset(&self->keyboard));

				nk_group_end(ctx);
			}
		}
		nk_end(ctx);

		/* Render widget */
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(0, 0, 0, 1);

		/* Draw nuklear GUI elements */
		nk_sdl_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_MEMORY, MAX_ELEMENT_MEMORY);
		glViewport(0, 0, width, height);


		/* Swap buffers */
		SDL_GL_SwapWindow(win);
	}

	nk_sdl_shutdown();
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return NULL;
}

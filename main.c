#include <assert.h>
#include <errno.h>

#include <gpio.h>
#include <module.h>
#include <utils.h>

#include "kbd_7seg.h"
#include "devicetree.h"

static const char *_compatible[] = {
	"mos,kim-1-user-io",
	"elektor,junior-computer-user-io",
	NULL
};

__global
ModuleInfo __info = {
	.name = "kbd_7seg",
	.author = "Aerosnail",
	.major = VERSION_MAJOR,
	.minor = VERSION_MINOR,
	.patch = VERSION_PATCH,
	.compatible = _compatible
};

__global int
__init(void)
{
	return 0;
}

__global int
__parse_dtb_prop(void **dst, const struct dtb_property *prop, find_phandle_t find_phandle, void *find_phandle_ctx)
{
	struct kbd_7seg_specs *specs;

	if (!*dst) *dst = calloc(1, sizeof(*specs));
	specs = (struct kbd_7seg_specs*)*dst;

	return parse_dtb_prop(specs, prop, find_phandle, find_phandle_ctx);
}

__global int
__create_instance(void **dst, ModuleSpecs *specs)
{
	struct kbd_7seg_specs *custom_specs = (struct kbd_7seg_specs*)specs->ctx;
	struct kbd_7seg *self;
	size_t i;
	int ret;


	if (!custom_specs) return EINVAL;

	ret = kbd_7seg_init(&self, specs->name);

	/* Connect gpios */
	for (i = 0; i < LEN(self->mux_sel); i++) {
		ret |= gpio_connect(self->mux_sel[i], custom_specs->mux_sel_dst[i]);
	}
	for (i = 0; i < LEN(self->mux_data); i++) {
		ret |= gpio_connect(self->mux_data[i], custom_specs->mux_data_dst[i]);
	}

	free(specs->ctx);
	if (ret) return ret;

	*dst = self;
	return 0;
}

__global int
__delete_instance(void *ctx)
{
	struct kbd_7seg *self = (struct kbd_7seg*)ctx;

	return kbd_7seg_deinit(self);
}

__global int
__deinit(void)
{
	return 0;
}

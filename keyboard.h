#pragma once

#include <stdint.h>
#include <stdbool.h>

struct keyboard {
	int matrix[3][7];
	bool reset;
};

int keyboard_init(struct keyboard *self, const char *name);
int keyboard_deinit(struct keyboard *self);
int keyboard_render(struct keyboard *self, struct nk_context *ctx);

int keyboard_get_state(struct keyboard *self, int row, int col);
int keyboard_get_reset(struct keyboard *self);

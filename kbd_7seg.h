#pragma once

#include <pthread.h>
#include <stdbool.h>

#include <module.h>
#include <gpio.h>

#include "display.h"
#include "keyboard.h"

struct kbd_7seg_specs {
	GPIO *mux_sel_dst[4];
	GPIO *mux_data_dst[7];
};

struct kbd_7seg {
	struct display display;
	struct keyboard keyboard;

	uint8_t selector;
	uint8_t data;

	GPIO *mux_sel[4];
	GPIO *mux_data[7];
	GPIO *reset;

	pthread_t tid;
	bool running;
};

int kbd_7seg_init(struct kbd_7seg **dst, const char *module_name);
int kbd_7seg_deinit(struct kbd_7seg *self);

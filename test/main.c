#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "gui/glad/glad.h"
#include "gui/nuklear/nuklear.h"
#include "gui/nuklear/nuklear_sdl_gl3.h"
#include "gui/style.h"
#include "gui/widgets.h"

#define WINDOW_TITLE "demo"
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define FONT_SIZE 18

#define MAX_VERTEX_MEMORY (512 * 1024)
#define MAX_ELEMENT_MEMORY (128 * 1024)

int
main(int argc, char *argv[])
{
	struct nk_context *ctx;
	SDL_Window *win;
	SDL_GLContext glContext;
	SDL_Event evt;
	int width, height;
	int gl_error;
	const char *gl_version;

	/* Initialize SDL2 */
	SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "0");
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS)) {
		fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
		return 1;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	/* Create new SDL window */
	win = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
	                       WINDOW_WIDTH, WINDOW_HEIGHT,
	                       SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN |
	                       SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);
	if (!win) {
		fprintf(stderr, "SDL window creation failed: %s\n", SDL_GetError());
		return 1;
	}

	/* Create context and init GLAD */
	glContext = SDL_GL_CreateContext(win);
	if (!glContext) {
		fprintf(stderr, "SDL context creation failed: %s\n", SDL_GetError());
		return 1;
	}

	gladLoadGLES2Loader(SDL_GL_GetProcAddress);

	ctx = nk_sdl_init(win);
	gui_load_fonts(ctx, 1.0);
	gui_set_style_default(ctx);


	/* Check results */
	gl_version = (const char*)glGetString(GL_VERSION);
	if (gl_version) {
		printf("Created OpenGL context: %s\n", gl_version);
	} else {
		fprintf(stderr, "Unable to create OpenGL context: %s (OpenGL status %d)\n",
				SDL_GetError(), glGetError());
		return 1;
	}

	/* Get initial window width/height */
	SDL_GetWindowSize(win, &width, &height);

	bool running = true;
	while (running) {
		/* Just make sure that everything is still fine */
		gl_error = glGetError();
		if (gl_error) fprintf(stderr,"Detected OpenGL non-zero status: %d\n", gl_error);

		SDL_WaitEvent(&evt);

		/* Handle inputs */
		nk_input_begin(ctx);
		do {
			switch (evt.type) {
			case SDL_QUIT:
				running = false;
			default:
				break;
			}

			nk_sdl_handle_event(&evt);
		} while (SDL_PollEvent(&evt));
		nk_input_end(ctx);

		static int x = 0;

		if (nk_begin(ctx, "Address + data", nk_rect(50, 50, 1200, 1200),
			NK_WINDOW_BORDER |NK_WINDOW_MOVABLE | NK_WINDOW_CLOSABLE | NK_WINDOW_SCALABLE)) {

			nk_layout_row_dynamic(ctx, 30, 1);
			nk_slider_int(ctx, 0, &x, 0x7fffffff, 1);

			nk_layout_space_begin(ctx, NK_STATIC, 32, 64);

			nk_layout_space_push(ctx, nk_rect(0, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), x % 128);
			nk_layout_space_push(ctx, nk_rect(80, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), (x / 128) % 128);
			nk_layout_space_push(ctx, nk_rect(160, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), (x / 128 / 128) % 128);
			nk_layout_space_push(ctx, nk_rect(240, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), (x / 128 / 128 / 128) % 128);
			nk_layout_space_push(ctx, nk_rect(400, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), (x / 128 / 128 / 128 / 128) % 128);
			nk_layout_space_push(ctx, nk_rect(480, 0, 32, 64));
			draw_7seg(ctx, nk_widget_position(ctx), nk_widget_size(ctx), (x / 128 / 128 / 128 / 128 / 128) % 128);

			//nk_fill_triangle(cmd_buffer, 100, 100, 100, 200, 200, 200, nk_rgb(0, 0, 200));

		}


		nk_end(ctx);



		/* Update window size */
		SDL_GetWindowSize(win, &width, &height);

		/* Render widget */
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(0, 0, 0, 1);

		/* Draw nuklear GUI elements */
		nk_sdl_render(NK_ANTI_ALIASING_ON, MAX_VERTEX_MEMORY, MAX_ELEMENT_MEMORY);
		glViewport(0, 0, width, height);


		/* Swap buffers */
		SDL_GL_SwapWindow(win);
	}

	nk_sdl_shutdown();
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

#include <string.h>

#include <utils.h>

#include "display.h"
#include "gui/widgets.h"

#define DIGIT_WIDTH 32
#define DIGIT_HEIGHT 64
#define DIGIT_SPACING (DIGIT_WIDTH / 4)

#define DECAY 0.95f

int
display_init(struct display *self, const char *name)
{
	(void)name;
	size_t i, j;

	memset(self->data, 0, sizeof(self->data));

	for (i=0; i<LEN(self->segments); i++) {
		for (j=0; j<LEN(self->segments[0]); j++) {
			self->segments[i][j] = 1;
		}
	}

	self->persistence = DECAY;

	return 0;
}

int
display_deinit(struct display *self)
{
	(void)self;
	return 0;
}

int
display_render(struct display *self, struct nk_context *ctx)
{
	size_t i, j;

	/* Draw persistence slider */
	nk_layout_row_dynamic(ctx, 30, 1);
	nk_slider_float(ctx, 0.f, &self->persistence, 1.0, 0.01f);

	/* Draw digits */
	nk_layout_space_begin(ctx, NK_STATIC, DIGIT_WIDTH, DIGIT_HEIGHT);
	for (i=0; i<LEN(self->data); i++) {
		nk_layout_space_push(ctx, nk_rect((i * 5 / 4) * (2 * DIGIT_WIDTH + DIGIT_SPACING),
		                                  0,
		                                  DIGIT_WIDTH,
		                                  DIGIT_HEIGHT)
		);

		/* Exponentially decay segment brightness (simulating persistence of
		 * vision to avoid excessive flickering) */
		for (j = 0; j < LEN(self->segments[i]); j++) {
			self->segments[i][j] = self->persistence * self->segments[i][j]
			                     + ((self->data[i] >> j) & 0x1);
		}

		draw_7seg_float(ctx, nk_widget_position(ctx), nk_widget_size(ctx), self->segments[i]);
	}

	return 0;
}
